﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OrderEntry.Domain.Core
{
    public enum PaymentMethod
    {
        Card, Cash
    }
    public class Order
    {
        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public bool Paid { get; set; }
        public PaymentMethod PaidBy { get; set; }

        public Customer Customer { get; set; }
        public virtual ICollection<OrderItems> OrderItems { get; set; }

        public Order()
        {
            OrderItems = new List<OrderItems>();
        }
    }
}
