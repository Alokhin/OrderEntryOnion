﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrderEntry.Domain.Core
{
    public class OrderItems
    {
        [Key]
        public int Id { get; set; }
        public string Item { get; set; }
        public decimal Price { get; set; }
        public int Count { get; set; }

        public int OrderId { get; set; }
        public virtual Order Order { get; set; }

        public decimal TotalPrice
        {
            get => Price * Count;
        }
    }
}
