﻿using System.ComponentModel.DataAnnotations.Schema;

namespace OrderEntry.Domain.Core
{
    [ComplexType]
    public class Customer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
