﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using OrderEntry.Domain.Core;
using OrderEntry.Domain.Interfaces;

namespace OrderEntry.Infrastructure.Data.Repositories
{
    public class OrderItemsRepository : IOrderItemsRepository
    {
        private readonly OrderContext _context;

        public OrderItemsRepository(OrderContext context)
        {
            _context = context;
        }

        public IEnumerable<OrderItems> GetAll()
        {
            return _context.OrderItems;
        }

        public OrderItems Get(int id)
        {
            throw new System.NotImplementedException();
        }

        public void Create(OrderItems item)
        {
            throw new System.NotImplementedException();
        }

        public void Update(OrderItems item)
        {
            throw new System.NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public void Save()
        {
            throw new System.NotImplementedException();
        }
    }
}
