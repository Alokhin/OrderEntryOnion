﻿using System.Collections.Generic;
using System.Data.Entity;
using OrderEntry.Domain.Core;
using OrderEntry.Domain.Interfaces;

namespace OrderEntry.Infrastructure.Data.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly OrderContext _context;

        public OrderRepository(OrderContext context)
        {
            _context = context;
        }

        public IEnumerable<Order> GetAll()
        {
            return _context.Orders;
        }

        public IEnumerable<Order> GetAllWithItems()
        {
            return _context.Orders.Include(nameof(Order.OrderItems));
        }
        public Order Get(int id)
        {
            return _context.Orders.Find(id);
        }

        public void Create(Order item)
        {
            _context.Orders.Add(item);
        }

        public void Update(Order item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var item = _context.Orders.Find(id);
            _context.Orders.Remove(item);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
