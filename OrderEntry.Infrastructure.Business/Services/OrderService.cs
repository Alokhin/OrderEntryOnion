﻿using System.Collections.Generic;
using OrderEntry.Domain.Core;
using OrderEntry.Domain.Interfaces;

namespace OrderEntry.Infrastructure.Business.Services
{
    public class OrderService
    {
        private readonly IOrderRepository _orderRepository;
        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public void UpdateAllChanges(IEnumerable<Order> orders)
        {
            foreach (var order in orders)
            {
                _orderRepository.Update(order);
            }
            _orderRepository.Save();
        }

        public void Update(Order order)
        {
            var entity = _orderRepository.Get(order.Id);
            if (entity == null)
            {
                return;;
            }

            entity.Date = order.Date;
            entity.Paid = order.Paid;
            entity.PaidBy = order.PaidBy;
            // Todo: order items update
        }

        public IEnumerable<Order> GetAll()
        {
            return _orderRepository.GetAllWithItems();
        }
    }
}
