﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderEntry.Domain.Core;
using OrderEntry.Infrastructure.Business.Services;
using OrderEntry.Infrastructure.Data;
using OrderEntry.Infrastructure.Data.Repositories;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new OrderContext())
            {
                var rep = new OrderRepository(db);
                var service = new OrderService(rep);
                var order = new Order()
                {
                    Customer = new Customer() {FirstName = "ds", LastName = "ds"},
                    Date = new DateTime(1998, 4, 5),
                    OrderItems = new List<OrderItems>()
                    {
                        new OrderItems() {Count = 3, Item = "Coca-Cola", Price = 1.99m},
                        new OrderItems() {Count = 5, Item = "Pepsi-Cola", Price = 1.49m}
                    }
                };
                rep.Create(order);
                rep.Save();
//            service.UpdateAllChanges(new List<Order>(){order});
            }
            //            using (var db = new OrderContext())
            //            {
            //                var rep = new OrderRepository(db);
            //                var service = new OrderService(rep);
            //                var orders = service.GetAll();
            //                var firstOrder = orders.FirstOrDefault();
            //
            //                firstOrder.Customer.LastName = "Smith";
            //                firstOrder.Customer.FirstName = "John";
            //                firstOrder.OrderItems.Add(new OrderItems() { Count = 30, Item = "Fanta", Price = 1.99m });
            //
            //                service.UpdateAllChanges(new List<Order>(){firstOrder});
            //                //                service.UpdateAllChanges(new List<Order>(){order});
            //            }
//            using (var db = new OrderContext())
//            {
//                var rep = new OrderRepository(db);
//                var service = new OrderService(rep);
//                var order = new Order()
//                {
//                    Id = 2,
//                    Customer = new Customer() {FirstName = "Anton", LastName = "Alokhin"},
//                    Date = new DateTime(2008, 7, 7),
//                    OrderItems = new List<OrderItems>()
//                    {
//                        new OrderItems() {Count = 30, Item = "Coca-Cola", Price = 1.99m},
//                        new OrderItems() {Count = 55, Item = "Pepsi-Cola", Price = 0.49m}
//                    }
//                };
//                var order2 = new Order()
//                {
//                    Id = 3,
//                    Customer = new Customer() { FirstName = "Ivan", LastName = "Ivanov" },
//                    Date = new DateTime(2008, 7, 7),
//                    OrderItems = new List<OrderItems>()
//                    {
//                        new OrderItems() {Count = 30, Item = "Coca-Cola", Price = 1.99m},
//                        new OrderItems() {Count = 55, Item = "Pepsi-Cola", Price = 0.49m}
//                    }
//                };
//                db.Entry(order).State =
//                    EntityState.Detached;
////                db.Orders.Attach(order);
////                service.UpdateAllChanges(new List<Order>(){or});
//                db.SaveChanges();
//            }
        }
    }
}
