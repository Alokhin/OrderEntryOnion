﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderEntry.Domain.Core;

namespace OrderEntry.Domain.Interfaces
{
    public interface IOrderItemsRepository
    {
        IEnumerable<OrderItems> GetAll();
        OrderItems Get(int id);
        void Create(OrderItems item);
        void Update(OrderItems item);
        void Delete(int id);
        void Save();
    }
}
