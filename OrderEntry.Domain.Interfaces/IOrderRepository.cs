﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderEntry.Domain.Core;

namespace OrderEntry.Domain.Interfaces
{
    public interface IOrderRepository
    {
        IEnumerable<Order> GetAll();
        IEnumerable<Order> GetAllWithItems();
        Order Get(int id);
        void Create(Order item);
        void Update(Order item);
        void Delete(int id);
        void Save();
    }
}
